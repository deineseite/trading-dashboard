import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-trading-dashboard',
  templateUrl: './trading-dashboard.component.html',
  styleUrls: ['./trading-dashboard.component.css']
})
export class TradingDashboardComponent implements OnInit {

  // New Connect, https://newconnect.pl/statystyki-okresowe, download one day (xls)
  public listOfSymbols = ['01C', '2CP', '4MS', '4MB', '7FT', '7LV', 'AAS', 'ABK', 'AIN', 'ACA', 'ACK', 'ADX', 'AVE', 'AER', 'AFH', 'AGL', 'AGP', 'AIT', 'AFC', 'ALD', 'ALU', 'AOL', 'APA', 'ASA', 'APC', 'APS', 'AQU', 'AQA', 'AQT', 'ARE', 'ARI', 'ARX', 'ARG', 'TYP', 'ASR', 'ATA', 'ATJ', 'ATO', 'ASP', 'AUX', 'AVT', 'AZC', 'BCI', 'BLT', 'BBA', 'BRH', 'BTX', 'BHX', 'BER', 'BFC', 'BGD', 'BEP', 'BMX', 'BIP', 'BTK', 'BPN', 'BLR', 'BTC', 'BLO', 'BTG', 'BLU', 'BPC', 'BPX', 'BSA', 'BDG', 'BVT', 'CAM', 'CBD', 'CRB', 'CAI', 'CRC', 'CSR', 'CCS', 'CDA', 'CTF', 'CRP', 'CFG', 'CFS', 'CHP', 'CLD', 'CMI', 'CDT', 'CLC', 'CMC', 'CRS', 'COR', 'CTE', 'CWP', 'BRO', 'DNS', 'DBE', 'DCD', 'DDI', 'DKR', 'DAM', 'DEG', 'DEV', 'DGN', 'DTX', 'DLK', 'DOK', 'DRG', 'DGE', 'DRF', 'DUA', 'ECL', 'EC2', 'ECC', 'ECR', 'ECO', 'EDN', 'EFE', 'EKS', 'EBX', 'EPR', 'EGH', 'ELQ', 'EMM', 'ELM', 'EMA', 'EMU', 'EON', 'ESK', 'ERH', 'ECK', 'ETX', 'EXA', 'EXC', 'EXM', 'FKD', 'FLG', 'F51', 'FHD', 'FIG', 'FTH', 'FIV', 'FTN', 'FOR', 'FPO', 'FVE', 'GAL', 'GMV', 'GNR', 'GEN', 'GMT', 'GNG', 'GX1', 'GTP', 'GTS', 'GKS', 'GOL', 'GTF', 'GOV', 'GRE', 'GRM', 'GME', 'GEM', 'HRC', 'GRC', 'GTY', 'H4F', 'MRH', 'HMP', 'HRL', 'HOR', 'HUB', 'HRT', 'HPS', 'IBC', 'ICD', 'IDH', 'IGT', 'IPW', 'IMG', 'ICA', 'IVO', 'INS', 'IST', 'IFA', 'IGN', 'IGS', 'ITL', 'INT', 'INM', 'IVE', 'INW', 'IOD', 'ISG', 'IUS', 'JRH', 'JJB', 'JWA', 'KPI', 'KBJ', 'KBT', 'KIN', 'KLN', 'KME', 'K2P', 'KOR', 'KPC', 'LCN', 'LAN', 'LPS', 'LEG', 'LET', 'LGT', 'LTG', 'LKS', 'LBD', 'LSH', 'LUG', 'LUK', 'M4B', 'MAD', 'MLB', 'MMA', 'MRK', 'MAX', 'MXP', 'MBF', 'MDA', 'MRD', 'MDP', 'MDN', 'MDB', 'MNS', 'MER', 'MEI', 'MRG', 'MFD', 'MMD', 'MLP', 'MTN', 'MND', 'MMC', 'MOE', 'MCE', 'MLT', 'MTR', 'MOV', 'MPY', 'VER', 'MSM', 'MTS', 'YAN', 'NST', 'NTW', 'NRS', 'NXB', 'NFP', 'NGG', 'NTS', 'NVV', 'NOV', 'NWA', 'OML', 'ONC', 'OPT', 'ORG', 'ORL', 'OUT', 'OVI', 'OXY', 'OZE', 'P2C', 'PTE', 'PRN', 'PAS', 'PFD', 'PBT', 'PFM', 'P2B', 'PIG', 'PNW', 'PSM', 'PLI', 'PLG', 'PNT', 'PIT', 'PLM', 'PTN', 'PSH', 'PBB', 'PFG', 'P24', 'PST', 'PRO', 'PLE', 'PRS', 'PTW', 'PYL', 'PDG', 'QRT', 'QON', 'QUB', 'RDG', 'RDS', 'REM', 'RSP', 'RBS', 'RCA', 'ROV', 'RCW', 'S4E', 'SKN', 'SCP', 'SEV', 'SFD', 'SFN', 'SFK', 'SWK', 'SIM', 'SMT', 'SMS', 'SBE', 'SIN', 'SOK', 'SPK', 'SPR', 'STD', 'STA', 'SCS', 'STI', 'SUL', 'SDG', 'SUN', 'SSK', 'SWT', 'SYM', 'SNG', 'SZR', 'TOS', 'TXN', 'TXF', 'TIG', 'TMP', 'TLO', 'TLS', 'TLG', 'TLV', 'THG', 'T2P', 'TME', 'THD', 'TLT', 'TGS', 'ECA', 'UFC', 'UNL', 'UTD', 'VAB', 'VKT', 'VAR', 'VEE', 'VRB', 'VIA', 'VDS', 'VIV', 'WHH', 'WRE', 'WRL', 'WOD', 'XBS', 'XPL', 'YOS'];
  //public listOfSymbols = ['01C', '2CP',];

  constructor() {
    this.addPrefixToEverySymbol('NEWCONNECT:');
  }
  
  ngOnInit() {
  }

  private addPrefixToEverySymbol(prefix: string): void {
    this.listOfSymbols = this.listOfSymbols.map(item => {
      return prefix + item;
    });
  }

}
