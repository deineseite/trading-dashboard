import { AfterViewInit, Component, ElementRef, Input, ViewChild } from '@angular/core';
declare const TradingView: any;
@Component({
    selector: 'rip-trading-view-fundamentals',
    templateUrl: './trading-view.component.html',
})

export class TradingViewFundamentalsComponent implements AfterViewInit {
    // allows for loading with any symbol
    @Input() _symbol: string = '';

    @Input()
    get symbol(): string {
        return this._symbol;
    }
    set symbol(symbol: string) {
        this._symbol = symbol;
        this.widgetId = `${encodeURI(this.symbol.replace(':', '_'))}_fundament`;
    }

    settings: any = {
        symbol: this.symbol,
        chartOnly: false,
        colorTheme: 'light',
        isTransparent: false,
        largeChartUrl: '',
        displayMode: 'regular',
        height: '100%',
        width: '100%',
        autosize: true,
        locale: 'pl'
    };

    widgetId: string = '';

    // wanted to be able to hide the widget if the symbol passed in was invalid (don't love their sad cloud face)
    @ViewChild('containerDiv', { static: false }) containerDiv: ElementRef;

    constructor(private _elRef: ElementRef) {
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.settings = {
                symbol: this.symbol,
                chartOnly: false,
                colorTheme: 'light',
                isTransparent: false,
                largeChartUrl: '',
                displayMode: 'regular',
                height: '100%',
                width: '100%',
                autosize: true
            };

            const script = document.createElement('script');
            script.src = 'https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js';
            //script.src = 'assets/js/embed-widget-mini-symbol-overview.js';
            script.async = true;
            script.type = "text/javascript";
            script.id = this.widgetId;
            script.innerHTML = JSON.stringify(this.settings);
            this.containerDiv.nativeElement.appendChild(script);
        }, 100);

    }

    /*ngAfterViewInit() {
        // need to do this in AfterViewInit because of the Input
        setTimeout(() => {
            this.widgetId = `${this.symbol}_fundamentals`;

            // postMessage listener for handling errors
            if (window.addEventListener) {
                window.addEventListener('message', (e: any) => {
                    if (e && e.data) {
                     //   console.log(e);
                        const payload = e.data;
                        // if the frameElementId is from this component, the symbol was no good and we should hide the widget
                        if (payload.name === 'tv-widget-no-data' && payload.frameElementId === this.widgetId) {
                            this.containerDiv.nativeElement.style.display = 'none';
                        }
                    }
                },
                    false,
                );
            }


            this.settings = {
                symbol: this.symbol,
                chartOnly: false,
                colorTheme: 'light',
                isTransparent: false,
                largeChartUrl: '',
                displayMode: 'regular',
                height: '100%',
                width: '100%',
                autosize: true,
                locale: 'pl',

            };
            const script = document.createElement('script');
            //script.src = 'https://s3.tradingview.com/external-embedding/embed-widget-financials.js';
            script.src = 'https://s3.tradingview.com/external-embedding/embed-widget-mini-symbol-overview.js';
            script.async = true;
            script.type = "text/javascript";
            script.id = this.widgetId;
            script.innerHTML = JSON.stringify(this.settings);
            this.containerDiv.nativeElement.appendChild(script);
            const brandingDiv = document.createElement('div');
            brandingDiv.innerHTML = `
     <div class="tradingview-widget-copyright">
     <a href="https://www.tradingview.com/symbols/${ this.symbol}/" rel="noopener" target="_blank">
     <span class="blue-text">${ this.symbol} Fundamental Data</span></a>
               by TradingView
           </div>
 `;

        });
    } */

}